//
//  Dish.swift
//  RestaurantDemo
//
//  Created by Cristian Olteanu on 14/03/2018.
//  Copyright © 2018 Cristian Olteanu. All rights reserved.
//

import UIKit

class Dish {
    var name: String
    var price: Int
    var image: UIImage
    var description: String

    init(name: String,
         price: Int,
         image: UIImage,
         description: String) {

        self.name = name
        self.price = price
        self.image = image
        self.description = description
    }
}

class DishSection {
    var name: String
    var dishes: [Dish]

    init(name: String,
         dishes: [Dish]) {

        self.name = name
        self.dishes = dishes
    }
}

func allDishSections() -> [DishSection] {

    let dish1 = Dish(name: "Red Soup", price: 30, image: UIImage(named: "redSoup")!, description: "Red Soup is BEST SOUP!")
    let dish2 = Dish(name: "Green Soup", price: 20, image: UIImage(named: "greenSoup")!, description: "Green Soup is BEST SOUP!")
    let dish3 = Dish(name: "Yellow Soup", price: 21, image: UIImage(named: "none")!, description: "Yellow Soup is BEST SOUP!")
    let dish4 = Dish(name: "Cream Soup", price: 40, image: UIImage(named: "creamSoup")!, description: "Cream Soup is BEST SOUP!")

    let soupsArray = [dish1, dish2, dish3, dish4]

    let soupsSection = DishSection(name: "Soups", dishes: soupsArray)

    let dish5 = Dish(name: "Pasta", price: 40, image: UIImage(named: "pasta")!, description: longDescription())
    let dish6 = Dish(name: "Sandwich", price: 30, image: UIImage(named: "sandwich")!, description: longDescription())
    let dish7 = Dish(name: "Pizza", price: 80, image: UIImage(named: "pizza")!, description: longDescription())
    let dish8 = Dish(name: "Grilled Chicken", price: 90, image: UIImage(named: "none")!, description: longDescription())
    let dish9 = Dish(name: "Steak", price: 100, image: UIImage(named: "steak")!, description: longDescription())

    let solidsArray = [dish5, dish6, dish7, dish8, dish9]

    let solidsSection = DishSection(name: "Solids", dishes: solidsArray)

    return [soupsSection, solidsSection]
}

func longDescription() -> String {
    return "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
}
